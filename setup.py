from setuptools import setup

setup(
    name='aspyre-srv-aiohttp',
    version='0.1.0',

    install_requires=[
        'aiohttp>=3.1.0',
    ],

    author='Aspyre Framework',
    author_email='server+aiohttp@aspy.re',
    description='Interface for connecting Aspyre to the aiohttp web server.',
    license='CC0',
    keywords='aspyre aiohttp server',
    url='aspy.re',

    packages=['aspyre.servers.aiohttp']
)
