#!/usr/bin/env python

import sys

import __init__ as aiohttp_server

class app():
    async def __call__(self, *args, **kwargs):
        return 200, {'Content-Type': 'text/plain', 'Server': 'Aspyre'}, b'Hello world!'

a = aiohttp_server.AIOHTTPServer(application=app())
a(host='127.0.0.1', port=sys.argv[1] if len(sys.argv) > 1 else 8000)
#cProfile.run("a(host='unix:///var/run/aspyre.sock')")
