from asyncio import get_event_loop

from urllib.parse import parse_qs
from functools import partial

from aiohttp import web

from aspyre import AspyreApplication


__all__ = ['AIOHTTPServer']


class AIOHTTPServer:
    name = 'aiohttp'  # Friendly name for Aspyre.
    version = {  # Version for this plug.
        'major': 0,
        'minor': 1,
        'patch': 0,
    }

    def __init__(self, application=None, loop=None):
        if not application or not isinstance(application, AspyreApplication):
            raise ValueError('No AspyreApplication given to the AIOHTTPServer constructor.')

        self.__application__ = application
        self.__loop__ = None
        self.__server__ = None

        self.set_loop(loop)


    def __call__(self, host=None, port=None):
        self.__server__ = web.Server(self.handler)

        # Check and create either a) UNIX file socket, or b) regular IP:Port socket.
        if host.lower().startswith('unix://'):
            host = host[6:]
            self.__asyncio_server__ = self.__loop__.create_unix_server(self.__server__, path=host)
        else:
            self.__asyncio_server__ = self.__loop__.create_server(self.__server__, host=host, port=port)

        try:
            self.__loop__.run_until_complete(
                    self.__asyncio_server__
            )

            self.__loop__.run_forever()
        except KeyboardInterrupt:
            pass

        self.__asyncio_server__.close()
        self.__loop__.close()

    async def handler(self, request):
        host = request.host
        path = request.path
        method = request.method.lower()

        body = None
        if request.can_read_body:
            body = await request.read()

        headers = request.raw_headers
        query_string = parse_qs(request.query_string, keep_blank_values=True) if request.query_string else {}

        # Additional, aiohttp-specific stuff we would like to expose to Aspyre users via the context argument.
        context = {
            'websocket': partial(web.WebSocketResponse, request),
            'stream': web.StreamResponse,
        }

        # Make the call, get a response.
        response = await self.__application__(host, path, method, headers, body, query_string, context)
        if isinstance(response, web.WebSocketResponse):  # Special handling for a WebSocket response.
            return response
        elif isinstance(response, web.StreamResponse):  # Special handling for a streaming response.
            return response
        else:
            status, headers, body = response

            aio_resp = web.Response(body=body, headers=headers)

            return aio_resp

    def set_application(self, application):
        if not application:
            raise ValueError('No AspyreApplication given to AIOHTTPServer.set_application.')

        self.__application__ = application

    def set_loop(self, loop):
        if self.__loop__ and self.__loop__.is_running():
            self.__loop__.close()

        if not loop:
            self.__loop__ = get_event_loop()
        else:
            self.__loop__ = loop
