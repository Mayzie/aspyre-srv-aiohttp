# aiohttp Plug for Aspyre

This module allows the Aspyre framework to use aiohttp as its HTTP server.

This module lets developers of Aspyre applications to have WebSocket and Multi-part Streaming response
functionality in their web applications.
